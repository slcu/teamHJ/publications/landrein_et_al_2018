## Introduction

This repository points to scripts and software neede to do the quantitative analysis 
in the paper

Nitrate modulates stem cell dynamics in Arabidopsis shoot meristems through cytokinins
Benoit Landrein, Pau Formosa-Jordan, Alice Malivert, Christoph Schuster, Charles W. Melnyk, 
Weibing Yang, Colin Turnbull, Elliot M. Meyerowitz, James C. W. Locke, and Henrik Jönsson
PNAS February 6, 2018 115 (6) 1382-1387; https://doi.org/10.1073/pnas.1718670115

## Scripts

The quantitative analysis is using the [RegionsAnalysis](gitlab.com/slcu/teamhj/pau/regionsanalysis)
software. Currently, the scripts connected to the Landrein manuscript are found in 
the [sub folder Applications of this repository](https://gitlab.com/slcu/teamHJ/pau/RegionsAnalysis/tree/master/Applications/Landrein_et_al_2018).

## Contact

henrik.jonsson@slcu.cam.ac.uk

pau.formosa-jordan@slcu.cam.ac.uk